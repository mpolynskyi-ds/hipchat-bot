package main

import (
	"encoding/json"
	"flag"
	"html/template"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"

	"./util"

	"github.com/gorilla/mux"
	"github.com/grokify/html-strip-tags-go"
	"github.com/tbruyelle/hipchat-go/hipchat"
	"github.com/davecgh/go-spew/spew"

	"html"
	"math/rand"
	"time"
	"./basic-bitbucket"
	"fmt"
)

func init() {
	basic_bitbucket.StartUpdateTask()
}

// RoomConfig holds information to send messages to a specific room
type RoomConfig struct {
	token *hipchat.OAuthAccessToken
	hc    *hipchat.Client
	name  string
}

// Context keep context of the running application
type Context struct {
	baseURL string
	static  string
	//rooms per room OAuth configuration and client
	rooms map[string]*RoomConfig
}

func (c *Context) healthcheck(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode([]string{"OK"})
}

func (c *Context) atlassianConnect(w http.ResponseWriter, r *http.Request) {
	lp := path.Join("./static", "atlassian-connect.json")
	vals := map[string]string{
		"LocalBaseUrl": c.baseURL,
	}
	tmpl, err := template.ParseFiles(lp)
	if err != nil {
		log.Fatalf("%v", err)
	}
	tmpl.ExecuteTemplate(w, "config", vals)
}

func (c *Context) installable(w http.ResponseWriter, r *http.Request) {
	authPayload, err := util.DecodePostJSON(r, true)
	if err != nil {
		log.Fatalf("Parsed auth data failed:%v\n", err)
	}

	credentials := hipchat.ClientCredentials{
		ClientID:     authPayload["oauthId"].(string),
		ClientSecret: authPayload["oauthSecret"].(string),
	}
	roomName := strconv.Itoa(int(authPayload["roomId"].(float64)))
	newClient := hipchat.NewClient("")
	tok, _, err := newClient.GenerateToken(credentials, []string{hipchat.ScopeSendNotification})
	if err != nil {
		log.Fatalf("Client.GetAccessToken returns an error %v", err)
	}

	util.SaveRoomToken("rooms_tokens", roomName, tok)

	rc := &RoomConfig{
		name: roomName,
		hc:   tok.CreateClient(),
	}
	c.rooms[roomName] = rc

	util.PrintDump(w, r, false)
	json.NewEncoder(w).Encode([]string{"OK"})
}

func (c *Context) config(w http.ResponseWriter, r *http.Request) {
	signedRequest := r.URL.Query().Get("signed_request")
	lp := path.Join("./static", "layout.hbs")
	fp := path.Join("./static", "config.hbs")
	vals := map[string]string{
		"LocalBaseUrl":  c.baseURL,
		"SignedRequest": signedRequest,
		"HostScriptUrl": c.baseURL,
	}
	tmpl, err := template.ParseFiles(lp, fp)
	if err != nil {
		log.Fatalf("%v", err)
	}
	tmpl.ExecuteTemplate(w, "layout", vals)
}

func (c *Context) storyHook(w http.ResponseWriter, r *http.Request) {
	log.Println(r)
	payLoad, err := util.DecodePostJSON(r, true)
	if err != nil {
		log.Fatalf("Parsed auth data failed:%v\n", err)
	}
	roomID := strconv.Itoa(int((payLoad["item"].(map[string]interface{}))["room"].(map[string]interface{})["id"].(float64)))
	messageText := payLoad["item"].(map[string]interface{})["message"].(map[string]interface{})["message"].(string)

	util.PrintDump(w, r, true)

	log.Printf("Sending notification to %s\n", roomID)
	storyId, isStory := util.GetStoryIdFromMessage(messageText)
	rawStoryInfoXml, status := util.GetStoryInfo(storyId, isStory)
	spew.Dump(rawStoryInfoXml)
	spew.Dump(err)
	if status == 200 {
		story := util.GetParsedStory(rawStoryInfoXml)

		if err == nil {
			storyTitle := story.Attributes[0].Value
			storyTeam := story.Attributes[1].Value
			storyStatus := story.Attributes[2].Value
			storyPriority := story.Attributes[3].Value
			storyDescription := story.Attributes[4].Value
			storyCode := story.Attributes[5].Value
			splitFrom := story.Attributes[6].Value

			notifRq := buildStoryHipchatCard(storyDescription, storyTeam, storyStatus, storyPriority, storyCode, storyTitle, storyId, isStory, splitFrom)

			//debug printing json body
			parseJson, err := json.Marshal(notifRq)
			if err != nil {
				println(err)
			} else {
				println(string(parseJson))
			}

			if _, ok := c.rooms[roomID]; ok {
				_, err = c.rooms[roomID].hc.Room.Notification(roomID, notifRq)
				if err != nil {
					log.Printf("Failed to notify HipChat channel:%v\n", err)
				}
			} else {
				log.Printf("Room is not registered correctly:%v\n", c.rooms)
			}
		}
	}
}

func buildStoryHipchatCard(storyDescription string, storyTeam string, storyStatus string, storyPriority string, storyCode string, storyTitle string, storyId string, isStory bool, splitFrom string) *hipchat.NotificationRequest {
	stripedStoryDescription := strip.StripTags(html.UnescapeString(storyDescription))
	storyTitle = strip.StripTags(html.UnescapeString(storyTitle))
	if len(stripedStoryDescription) > 1000 {
		stripedStoryDescription = stripedStoryDescription[0:1000]
	}
	//construct guts of hipchat card
	storyCardDescription := hipchat.CardDescription{
		Format: "html",
		Value:  stripedStoryDescription,
	}
	storyCardTeam := hipchat.Attribute{
		Label: "Team",
		Value: hipchat.AttributeValue{
			//URL:   "",
			Style: "lozenge-complete",
			//Type:  "",
			Label: storyTeam,
			//Value: "",
			//Icon: &hipchat.Icon{
			//	URL:   "",
			//	URL2x: "",
			//},
		},
	}

	var statusStyle string
	if storyStatus == "Ready to Release" || storyStatus == "Pending Merge" || storyStatus == "Released" || storyStatus == "Resolved" {
		statusStyle = "lozenge-success"
	} else if storyStatus == "In Progress" {
		statusStyle = "lozenge-current"
	} else if storyStatus == "Split" {
		statusStyle = "lozenge-error"
	} else {
		statusStyle = "lozenge"
	}
	storyCardStatus := hipchat.Attribute{
		Label: "Status",
		Value: hipchat.AttributeValue{
			//URL:   "",
			Style: statusStyle,
			//Type:  "",
			Label: storyStatus,
			//Value: "",
			//Icon: &hipchat.Icon{
			//	URL:   "",
			//	URL2x: "",
			//},
		},
	}

	var priorityStyle string
	if storyPriority == "P1" {
		priorityStyle = "lozenge-error"
	} else {
		priorityStyle = "lozenge"
	}
	storyCardPriority := hipchat.Attribute{
		Label: "Priority",
		Value: hipchat.AttributeValue{
			//URL:   "",
			Style: priorityStyle,
			//Type:  "",
			Label: storyPriority,
			//Value: "",
			//Icon: &hipchat.Icon{
			//	URL:   "",
			//	URL2x: "",
			//},
		},
	}
	storyCardCode := hipchat.Attribute{
		Label: "Code",
		Value: hipchat.AttributeValue{
			//URL:   "",
			Style: "lozenge-complete",
			//Type:  "",
			Label: storyCode,
			//Value: "",
			//Icon: &hipchat.Icon{
			//	URL:   "",
			//	URL2x: "",
			//},
		},
	}
	versionOneIcon := &hipchat.Icon{
		//	URL:   "http://loremflickr.com/320/240/dog",
		URL: "https://www8.v1host.com/s/18.1.4.12/css/images/icons/favicon.ico",
		//URL2x: "",
	}


	attrs := []hipchat.Attribute {storyCardTeam, storyCardCode, storyCardStatus, storyCardPriority}

	if splitFrom != "N/A" {
		storyCardSplitFrom := hipchat.Attribute{
			Label: "Split From",
			Value: hipchat.AttributeValue{
				//URL:   "",
				Style: "lozenge",
				//Type:  "",
				Label: splitFrom,
				//Value: "",
				//Icon: &hipchat.Icon{
				//	URL:   "",
				//	URL2x: "",
				//},
			},
		}
		attrs = append(attrs, storyCardSplitFrom)
	}

	var pullRequestHTML string
	//var pullRequests []hipchat.Attribute

	res := basic_bitbucket.GetPRList(storyCode)
	if len(res) != 0 {
		pullRequestHTML = "<b>Pull request status:</b><br>"
	}
	spew.Dump(res)
	for key, value := range res {
		if value.Status {
			pullRequestHTML = pullRequestHTML + fmt.Sprintf("<a href='%s'>%s</a>", value.Link, key) + " ✅"+"<br>"
		} else {
			pullRequestHTML = pullRequestHTML + fmt.Sprintf("<a href='%s'>%s</a>", value.Link, key) + " 🔴"+"<br>"
		}
	}

	storyActivity := &hipchat.Activity{
		Icon: versionOneIcon,
		HTML: "<b>" + storyCode + "</b>" + " " + storyTitle + "<br>" + pullRequestHTML,
	}

	//for key, value := range res {
	//	if value {
	//		approvedPR := hipchat.Attribute{
	//			Label: key,
	//			Value: hipchat.AttributeValue{
	//				//URL:   "www.google.com",
	//				//Style: "lozenge-success",
	//				//Type:  "",
	//				Label: "✅",
	//				//Icon: &hipchat.Icon{
	//				//	URL:   "",
	//				//	URL2x: "",
	//				//},
	//			},
	//		}
	//		pullRequests = append(pullRequests, approvedPR)
	//	} else {
	//		notApprovedPR := hipchat.Attribute{
	//			Label: key,
	//			Value: hipchat.AttributeValue{
	//				//URL:   "",
	//				//Style: "lozenge-error",
	//				//Type:  "",
	//				Label: "🔴",
	//				//Value: "asdasdasd",
	//				//Icon: &hipchat.Icon{
	//				//	URL:   "",
	//				//	URL2x: "",
	//				//},
	//			},
	//		}
	//		pullRequests = append(pullRequests, notApprovedPR)
	//	}
	//
	//}
	//spew.Dump(pullRequests)

	//storyCardAttributes := append(attrs, pullRequests...)
	//spew.Dump(storyCardAttributes)

	rand.Seed(time.Now().UnixNano())
	num := rand.Int()
	storyCard := &hipchat.Card{
		Style:       "application",
		Description: storyCardDescription,
		Format:      "medium",
		//URL:        	"",
		Title: 		storyTitle,
		//Thumbnail:   	nil,
		Activity:   storyActivity,
		Attributes: attrs,
		ID:         storyId + "randStamp" + strconv.Itoa(num),
		//Icon:        	versionOneIcon,
	}

	var messageCardColor hipchat.Color

	if isStory{
		messageCardColor = "green"
	} else {
		messageCardColor = "red"
	}

	notifRq := &hipchat.NotificationRequest{
		Color:         	messageCardColor,
		Message:       	"<b>" + storyCode + "</b>" + " " + stripedStoryDescription,
		Notify:        	false,
		MessageFormat: 	"html",
		//From:          "",
		Card: 			storyCard,
	}
	return notifRq
}

// routes all URL routes for app add-on
func (c *Context) routes() *mux.Router {
	r := mux.NewRouter()
	//healthcheck route required by Micros
	r.Path("/healthcheck").Methods("GET").HandlerFunc(c.healthcheck)
	//descriptor for Atlassian Connect
	r.Path("/").Methods("GET").HandlerFunc(c.atlassianConnect)
	r.Path("/atlassian-connect.json").Methods("GET").HandlerFunc(c.atlassianConnect)

	// HipChat specific API routes
	r.Path("/installable").Methods("POST").HandlerFunc(c.installable)
	r.Path("/config").Methods("GET").HandlerFunc(c.config)
	r.Path("/story").Methods("POST").HandlerFunc(c.storyHook)

	r.PathPrefix("/").Handler(http.FileServer(http.Dir(c.static)))
	return r
}

func main() {
	var (
		port    = flag.String("port", os.Getenv("BASE_PORT"), "web server port")
		static  = flag.String("static", "./static/", "static folder")
		baseURL = flag.String("baseurl", os.Getenv("BASE_URL"), "local base url")
	)
	flag.Parse()

	c := &Context{
		baseURL: *baseURL,
		static:  *static,
		rooms:   make(map[string]*RoomConfig),
	}

	data := util.LoadTokens("rooms_tokens")

	for roomName, tok := range data {
		rc := &RoomConfig{
			name: roomName,
			hc:   tok.CreateClient(),
		}
		c.rooms[roomName] = rc
	}

	log.Printf("Base HipChat integration v0.10 - running on port:%v", *port)

	r := c.routes()
	http.Handle("/", r)
	http.ListenAndServe(":"+*port, nil)
}
