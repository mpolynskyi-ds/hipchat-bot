package util

import (
	"encoding/json"
	"log"
	"net/http"
	"net/http/httputil"
	"fmt"
	"io/ioutil"
	"net/url"
	"bytes"
	"regexp"
	"strings"
	"encoding/xml"
	//"github.com/davecgh/go-spew/spew"
	"github.com/vaughan0/go-ini"
	"os"
	"../basic-bitbucket"
	"github.com/tbruyelle/hipchat-go/hipchat"
	"encoding/gob"
)

var config ini.File

func init() {
	var err error
	config, err = ini.LoadFile("settings.ini")

	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}
	basic_bitbucket.Auth(GetConfig("bitbucket", "login"), GetConfig("bitbucket", "password"))
}

func GetConfig(section string, key string) string {
	val,_ := config.Get(section, key)
	return val
}

// PrintDump prints dump of request, optionally writing it in the response
func PrintDump(w http.ResponseWriter, r *http.Request, write bool) {
	dump, _ := httputil.DumpRequest(r, true)
	log.Printf("%v", string(dump))
	if write == true {
		w.Write(dump)
	}
}

// Decode into a ma[string]interface{} the JSON in the POST Request
func DecodePostJSON(r *http.Request, logging bool) (map[string]interface{}, error) {
	var err error
	var payLoad map[string]interface{}
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&payLoad)
	if logging == true {
		log.Printf("Parsed body:%v", payLoad)
	}
	return payLoad, err
}

func GetStoryIdFromMessage(message string) (string, bool) {
	var re = regexp.MustCompile(`oidToken=Story%3A\d+|oidToken=Story:\d+|oidToken=Defect%3A\d+|oidToken=Defect:\d+`)
	var isStory bool
	if strings.Contains(message, "oidToken=Story") {
		isStory = true
	} else {
		isStory = false
	}
	storyWithId := message[re.FindStringIndex(message)[0]:re.FindStringIndex(message)[1]]
	println(storyWithId)
	if strings.Contains(storyWithId, "%3A") {
		re = regexp.MustCompile("\\d+$")
	} else {
		re = regexp.MustCompile("[0-9]+")
	}
	id := re.FindAllString(storyWithId, -1)
	return strings.Join(id, ""), isStory
}

//get versionone story data (Name,Team.Name,Status.Name,Priority.Name,Description), returning string xml
func GetStoryInfo(storyId string, isStory bool) (string, int) {
	var status int
	params := url.Values{}
	body := bytes.NewBufferString(params.Encode())

	// Create client
	client := &http.Client{}

	// Create request
	var getUrl string

	if isStory {
		getUrl = "https://www8.v1host.com/DealerSocket/rest-1.v1/Data/Story/"
	} else {
		getUrl = "https://www8.v1host.com/DealerSocket/rest-1.v1/Data/Defect/"
	}
	req, err := http.NewRequest("GET", getUrl + storyId + "?sel=Name,Team.Name,Status.Name,Priority.Name,Description,Number,SplitFrom.Number", body)

	// Headers
	req.Header.Add("Authorization", GetConfig("versionone", "auth"))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")

	parseFormErr := req.ParseForm()
	if parseFormErr != nil {
		fmt.Println(parseFormErr)
	}

	// Fetch Request
	resp, err := client.Do(req)

	if err != nil {
		fmt.Println("Failure : ", err)
	}

	// Read Response Body
	respBody, _ := ioutil.ReadAll(resp.Body)

	isError := strings.Contains(string(respBody), "<Error href")

	if !isError {
		status = 200
	}

	return string(respBody), status
}



type Story struct {
	XMLName 	xml.Name `xml:"Asset"`
	Attributes  []Attributes `xml:"Attribute"`
}
type Attributes struct {
	Key   string `xml:"name,attr"`
	Value string `xml:",innerxml"`
}

func GetParsedStory(rawStoryInfoXml string) Story{
	log.Println("started parsing")
	var story Story

	xml.Unmarshal([]byte(rawStoryInfoXml), &story)
	for i := 0; i < len(story.Attributes); i++ {
		if story.Attributes[i].Value == "" {
			story.Attributes[i].Value = "N/A"
		}
	}

	return story
}

func SaveRoomToken(path string, roomName string, tok *hipchat.OAuthAccessToken) error {
	var rooms = LoadTokens(path)

	file, err := os.Create(path)
	encoder := gob.NewEncoder(file)
	rooms[roomName] = tok

	errEncode := encoder.Encode(rooms)
	if errEncode != nil {
		panic(errEncode)
	}

	return err
}

func LoadTokens(path string) map[string]*hipchat.OAuthAccessToken {
	var decodedMap = make(map[string]*hipchat.OAuthAccessToken)

	if _, err := os.Stat(path); err == nil {
		file, err := os.Open(path)
		stat, _ := file.Stat()

		if stat.Size() > 0 {
			decoder := gob.NewDecoder(file)
			err = decoder.Decode(&decodedMap)
			if err != nil {
				panic(err)
			}
		}
	}

	return decodedMap
}
