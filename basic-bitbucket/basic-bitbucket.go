package basic_bitbucket

import (
	"fmt"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"net/url"
	"log"
	"time"
	"strconv"
	"go.uber.org/ratelimit"
	"sync"
)

type basicAuth struct {
	user     string
	password string
}

type PullRequests struct {
	Next    string             `json:"next"`
	Page    int                `json:"page"`
	Pagelen int                `json:"pagelen"`
	Size    int                `json:"size"`
	Values  []PullRequestsItem `json:"values"`
}

type PullRequestsItem struct {
	ID int `json:"id"`
	Links struct {
		Activity struct {
			Href string `json:"href"`
		} `json:"activity"`
		Approve struct {
			Href string `json:"href"`
		} `json:"approve"`
		Comments struct {
			Href string `json:"href"`
		} `json:"comments"`
		Commits struct {
			Href string `json:"href"`
		} `json:"commits"`
		Decline struct {
			Href string `json:"href"`
		} `json:"decline"`
		Diff struct {
			Href string `json:"href"`
		} `json:"diff"`
		HTML struct {
			Href string `json:"href"`
		} `json:"html"`
		Merge struct {
			Href string `json:"href"`
		} `json:"merge"`
		Self struct {
			Href string `json:"href"`
		} `json:"self"`
		Statuses struct {
			Href string `json:"href"`
		} `json:"statuses"`
	} `json:"links"`
	Source struct {
		Branch struct {
			Name string `json:"name"`
		} `json:"branch"`
	} `json:"source"`

	Title string `json:"title"`
}

type PullRequest struct {
	MergeCommit interface{} `json:"merge_commit"`
	Description string      `json:"description"`
	Links struct {
		Decline struct {
			Href string `json:"href"`
		} `json:"decline"`
		Commits struct {
			Href string `json:"href"`
		} `json:"commits"`
		Self struct {
			Href string `json:"href"`
		} `json:"self"`
		Comments struct {
			Href string `json:"href"`
		} `json:"comments"`
		Merge struct {
			Href string `json:"href"`
		} `json:"merge"`
		HTML struct {
			Href string `json:"href"`
		} `json:"html"`
		Activity struct {
			Href string `json:"href"`
		} `json:"activity"`
		Diff struct {
			Href string `json:"href"`
		} `json:"diff"`
		Approve struct {
			Href string `json:"href"`
		} `json:"approve"`
		Statuses struct {
			Href string `json:"href"`
		} `json:"statuses"`
	} `json:"links"`
	Title             string `json:"title"`
	CloseSourceBranch bool   `json:"close_source_branch"`
	Reviewers []struct {
		Username    string `json:"username"`
		DisplayName string `json:"display_name"`
		Type        string `json:"type"`
		UUID        string `json:"uuid"`
		Links struct {
			Self struct {
				Href string `json:"href"`
			} `json:"self"`
			HTML struct {
				Href string `json:"href"`
			} `json:"html"`
			Avatar struct {
				Href string `json:"href"`
			} `json:"avatar"`
		} `json:"links"`
	} `json:"reviewers"`
	Destination struct {
		Commit struct {
			Hash string `json:"hash"`
			Links struct {
				Self struct {
					Href string `json:"href"`
				} `json:"self"`
			} `json:"links"`
		} `json:"commit"`
		Repository struct {
			Links struct {
				Self struct {
					Href string `json:"href"`
				} `json:"self"`
				HTML struct {
					Href string `json:"href"`
				} `json:"html"`
				Avatar struct {
					Href string `json:"href"`
				} `json:"avatar"`
			} `json:"links"`
			Type     string `json:"type"`
			Name     string `json:"name"`
			FullName string `json:"full_name"`
			UUID     string `json:"uuid"`
		} `json:"repository"`
		Branch struct {
			Name string `json:"name"`
		} `json:"branch"`
	} `json:"destination"`
	State    string      `json:"state"`
	ClosedBy interface{} `json:"closed_by"`
	Summary struct {
		Raw    string `json:"raw"`
		Markup string `json:"markup"`
		HTML   string `json:"html"`
		Type   string `json:"type"`
	} `json:"summary"`
	Source struct {
		Commit struct {
			Hash string `json:"hash"`
			Links struct {
				Self struct {
					Href string `json:"href"`
				} `json:"self"`
			} `json:"links"`
		} `json:"commit"`
		Repository struct {
			Links struct {
				Self struct {
					Href string `json:"href"`
				} `json:"self"`
				HTML struct {
					Href string `json:"href"`
				} `json:"html"`
				Avatar struct {
					Href string `json:"href"`
				} `json:"avatar"`
			} `json:"links"`
			Type     string `json:"type"`
			Name     string `json:"name"`
			FullName string `json:"full_name"`
			UUID     string `json:"uuid"`
		} `json:"repository"`
		Branch struct {
			Name string `json:"name"`
		} `json:"branch"`
	} `json:"source"`
	CommentCount int `json:"comment_count"`
	Author struct {
		Username    string `json:"username"`
		DisplayName string `json:"display_name"`
		Type        string `json:"type"`
		UUID        string `json:"uuid"`
		Links struct {
			Self struct {
				Href string `json:"href"`
			} `json:"self"`
			HTML struct {
				Href string `json:"href"`
			} `json:"html"`
			Avatar struct {
				Href string `json:"href"`
			} `json:"avatar"`
		} `json:"links"`
	} `json:"author"`
	CreatedOn time.Time `json:"created_on"`
	Participants []struct {
		Role           string    `json:"role"`
		ParticipatedOn time.Time `json:"participated_on"`
		Type           string    `json:"type"`
		Approved       bool      `json:"approved"`
		User struct {
			Username    string `json:"username"`
			DisplayName string `json:"display_name"`
			Type        string `json:"type"`
			UUID        string `json:"uuid"`
			Links struct {
				Self struct {
					Href string `json:"href"`
				} `json:"self"`
				HTML struct {
					Href string `json:"href"`
				} `json:"html"`
				Avatar struct {
					Href string `json:"href"`
				} `json:"avatar"`
			} `json:"links"`
		} `json:"user"`
	} `json:"participants"`
	Reason    string    `json:"reason"`
	UpdatedOn time.Time `json:"updated_on"`
	Type      string    `json:"type"`
	ID        int       `json:"id"`
	TaskCount int       `json:"task_count"`
}

type RepositoryResponse struct {
	Next    string       `json:"next"`
	Page    int          `json:"page"`
	Pagelen int          `json:"pagelen"`
	Size    int          `json:"size"`
	Values  []Repository `json:"values"`
}

type Repository struct {
	CreatedOn   string `json:"created_on"`
	Description string `json:"description"`
	ForkPolicy  string `json:"fork_policy"`
	FullName    string `json:"full_name"`
	HasIssues   bool   `json:"has_issues"`
	HasWiki     bool   `json:"has_wiki"`
	IsPrivate   bool   `json:"is_private"`
	Language    string `json:"language"`
	Links struct {
		Avatar struct {
			Href string `json:"href"`
		} `json:"avatar"`
		Branches struct {
			Href string `json:"href"`
		} `json:"branches"`
		Clone []struct {
			Href string `json:"href"`
			Name string `json:"name"`
		} `json:"clone"`
		Commits struct {
			Href string `json:"href"`
		} `json:"commits"`
		Downloads struct {
			Href string `json:"href"`
		} `json:"downloads"`
		Forks struct {
			Href string `json:"href"`
		} `json:"forks"`
		Hooks struct {
			Href string `json:"href"`
		} `json:"hooks"`
		HTML struct {
			Href string `json:"href"`
		} `json:"html"`
		Pullrequests struct {
			Href string `json:"href"`
		} `json:"pullrequests"`
		Self struct {
			Href string `json:"href"`
		} `json:"self"`
		Source struct {
			Href string `json:"href"`
		} `json:"source"`
		Tags struct {
			Href string `json:"href"`
		} `json:"tags"`
		Watchers struct {
			Href string `json:"href"`
		} `json:"watchers"`
	} `json:"links"`
	Mainbranch struct {
		Name string `json:"name"`
		Type string `json:"type"`
	} `json:"mainbranch"`
	Name string `json:"name"`
	Owner struct {
		DisplayName string `json:"display_name"`
		Links struct {
			Avatar struct {
				Href string `json:"href"`
			} `json:"avatar"`
			HTML struct {
				Href string `json:"href"`
			} `json:"html"`
			Self struct {
				Href string `json:"href"`
			} `json:"self"`
		} `json:"links"`
		Type     string `json:"type"`
		Username string `json:"username"`
		UUID     string `json:"uuid"`
	} `json:"owner"`
	Project struct {
		Key string `json:"key"`
		Links struct {
			Avatar struct {
				Href string `json:"href"`
			} `json:"avatar"`
			HTML struct {
				Href string `json:"href"`
			} `json:"html"`
			Self struct {
				Href string `json:"href"`
			} `json:"self"`
		} `json:"links"`
		Name string `json:"name"`
		Type string `json:"type"`
		UUID string `json:"uuid"`
	} `json:"project"`
	Scm       string `json:"scm"`
	Size      int    `json:"size"`
	Slug      string `json:"slug"`
	Type      string `json:"type"`
	UpdatedOn string `json:"updated_on"`
	UUID      string `json:"uuid"`
	Website   string `json:"website"`
}

type PullRequestStatus struct {
	Status bool
	Link   string
}

const approveThreshold = 1
const maxPageSize = "50"

const reposListUpdateInterval = 300

const apiUrl = "https://api.bitbucket.org/2.0"
const httpUrl = "https://bitbucket.org"

var repoOwner = "dealersocket"

var reposList []string

var a basicAuth

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

func Auth(user string, password string) {
	a.user = user
	a.password = password
}

func GetPRList(id string) map[string]PullRequestStatus {
	prUrl := "/repositories/%s/%s/pullrequests/"

	qs := url.Values{}
	qs.Add("q", fmt.Sprintf("source.branch.name ~ \"%s\"", id))

	result := make(map[string]PullRequestStatus, 50)

	var wg sync.WaitGroup

	var poolRequestList = map[string]PullRequestsItem{}

	rl := ratelimit.New(50)

	prev := time.Now()

	for _, repo := range reposList {

		now := rl.Take()

		wg.Add(1)
		go func(repo string, prev time.Time) {
			defer wg.Done()

			requestUrl := requestUrl(prUrl, qs, repoOwner, repo)

			response, errFetch := fetchResult(requestUrl)
			var pr PullRequests

			if errFetch == nil {
				err := json.Unmarshal(response, &pr)

				if err == nil && len(pr.Values) > 0 {
					poolRequestList[repo] = pr.Values[0]
				}
			} else {
				log.Println(errFetch)
			}

			prev = now
		}(repo, prev)


	}

	wg.Wait()

	var keys []string
	for idx := range poolRequestList {
		keys = append(keys, idx)
	}

	for _, repo := range keys {
		wg.Add(1)

		go func(repo string) {
			defer wg.Done()

			res := getPR(repo, poolRequestList[repo].ID)
			var status PullRequestStatus
			status.Status = isApproved(res)
			status.Link = fmt.Sprintf("%s/%s/%s/pull-requests/%s", httpUrl, repoOwner, repo, strconv.Itoa(poolRequestList[repo].ID))

			result[repo] = status
		}(repo)

	}
	wg.Wait()

	return result
}

func isApproved(pr PullRequest) bool {
	approveCount := 0

	for _, item := range pr.Participants {
		if item.Role == "REVIEWER" && item.Approved == true {
			approveCount++
			if approveCount == approveThreshold {
				return true
			}
		}
	}

	return false
}

func getPR(repoSlug string, id int) PullRequest {
	prUrl := "/repositories/%s/%s/pullrequests/%s"

	params := url.Values{}
	params.Add("pagelen", maxPageSize)

	requestUrl := requestUrl(prUrl, params, repoOwner, repoSlug, strconv.Itoa(id))
	response, errFetch := fetchResult(requestUrl)

	var pr PullRequest

	if errFetch == nil {
		err := json.Unmarshal(response, &pr)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		log.Println("Unable to fetch data", errFetch)
	}

	return pr
}

func fetchResult(requestUrl string) ([]byte, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", requestUrl, nil)
	req.SetBasicAuth(a.user, a.password)
	resp, err := client.Do(req)

	var bodyText []byte
	if err != nil || resp.StatusCode != 200 {
		return bodyText, fmt.Errorf("Response: %s", resp.Status)
	}

	bodyText, _ = ioutil.ReadAll(resp.Body)

	return bodyText, err
}

func requestUrl(template string, queryValues url.Values, args ...interface{}) string {

	qs := ""
	if len(queryValues) > 0 {
		qs = "?" + queryValues.Encode()
	}

	if len(args) == 1 && args[0] == "" {
		return apiUrl + template + qs
	}

	return apiUrl + fmt.Sprintf(template, args...) + qs
}

func fetchReposList() {
	prUrl := "/repositories/%s"

	params := url.Values{}
	params.Add("q", "project.key = \"WEBSITES\"")
	params.Add("pagelen", maxPageSize)

	var newList []string

	var loader = func(prUrl string, params url.Values) RepositoryResponse {
		requestUrl := requestUrl(prUrl, params, repoOwner)
		response, errFetch := fetchResult(requestUrl)

		var rr RepositoryResponse

		if errFetch == nil {
			err := json.Unmarshal(response, &rr)
			if err != nil {
				log.Println(err)
			}
		}

		return rr
	}

	result := loader(prUrl, params)
	for _, item := range result.Values {
		newList = append(newList, item.Name)
	}

	curResultsCount := 0
	for curResultsCount < result.Size {
		params.Add("page", strconv.Itoa(result.Page+1))
		result := loader(prUrl, params)
		curResultsCount = result.Page * result.Pagelen

		if len(result.Values) > 0 {
			for _, item := range result.Values {
				newList = append(newList, item.Name)
			}
		}
	}

	reposList = newList
}

func StartUpdateTask() {
	fetchReposList()

	log.Println(fmt.Sprintf("Loaded repos: %d", len(reposList)))

	ticker := time.NewTicker(time.Second * reposListUpdateInterval)
	go func() {
		for {
			select {
			case <-ticker.C:
				fetchReposList()
			}
		}
	}()
}